__author__ = 'Andrey Bogoyavlensky'
import hashlib
import operator
import copy


class Coder(object):

    def get_encode_text(self, source_text, range_source, keyword=None):
        """Method for encode source text(unicode) to MyCode.
        input: str from unicode
        output: str with encoded text"""
        keyword = self.get_keyword(keyword)
        self.range_source = range_source
        list_of_int = self._str_to_int(source_text)
        self.list_of_bin = self._int_to_bin(list_of_int)
        self.list_of_bin_wthout_key = copy.deepcopy(self.list_of_bin)
        if keyword:
            self.list_of_bin = keyword + self.list_of_bin
        self.list_of_mybin = self._bin_to_mybin(self.list_of_bin)
        if keyword:
            for i in xrange(len(keyword)):
                self.list_of_mybin.pop(0)
        list_of_new_int = self._bin_to_int(self.list_of_mybin)
        encode_text = self._int_to_str(list_of_new_int)
        return encode_text

    def get_decode_text(self, encode_text, keyword=None):
        """ input:  str with encoded text
            output: str from unicode with true(decoded) text"""
        list_of_int = self._str_to_int(encode_text)
        list_of_bin = self._int_to_bin(list_of_int)
        keyword_new = []
        if keyword:
            keyword_new = self.get_keyword(keyword)
            keyword_new = self._bin_to_mybin(keyword_new)
            list_of_bin = keyword_new + list_of_bin
        list_of_bin_new = self._mybin_to_bin(list_of_bin)
        if keyword_new:
            for i in xrange(len(keyword_new)):
                list_of_bin_new.pop(0)
        list_of_new_int = self._bin_to_int(list_of_bin_new)
        decode_text = self._int_to_str(list_of_new_int)
        return decode_text

    # counting statistic
    def get_zero_distribution(self, item='0'):
        if self.list_of_mybin:
            result = {'encoded': {'val': [], 'count': [], 'name': 'Encoded text'},
                      'source': {'val': [], 'count': [], 'name': 'Source text'}}
            bits = ''
            for i in self.list_of_mybin:
                bits += i
            length = len(bits)

            bits_source = ''
            for i in self.list_of_bin:
                bits_source += i
            step = length / 20
            count = 0
            while count <= length:
                if count > 0:
                    result['encoded']['val'].append(round(float(bits[:count].count(item)) / float(count), 3))
                    result['source']['val'].append(round(float(bits_source[:count].count(item)) / float(count), 3))
                else:
                    result['encoded']['val'].append(0)
                    result['source']['val'].append(0)
                result['encoded']['count'].append(count)
                result['source']['count'].append(count)
                count += step
        else:
            result = {}
        return result

    def get_bit_distribution(self, bit_list, name):
        if self.list_of_mybin:
            result = {'encoded': {'point': bit_list, 'val': [], 'name': '{0} in encoded text'.format(name)},
                      'source': {'point': bit_list, 'val': [], 'name': '{0} in source text'.format(name)}
            }
            bits = ''
            for i in self.list_of_mybin:
                bits += i
            length = len(bits)
            bits_source = ''
            for i in self.list_of_bin:
                bits_source += i
            for i in result['encoded']['point']:
                # result['encoded']['val'].append(bits.count(i))
                # result['source']['val'].append(bits_source.count(i))
                j = 0
                step = len(bit_list[0])
                m_count = 0
                m_count_source = 0
                while j < length - step:
                    m_en = bits[j:j + step]
                    m_source = bits_source[j:j + step]
                    if m_en == i:
                        m_count += 1
                    if m_source == i:
                        m_count_source += 1
                    j += step
                result['encoded']['val'].append(m_count)
                result['source']['val'].append(m_count_source)
        else:
            result = {}
        return result

    def analys_statistic(self):
        dict_analys = {'source': {'data': self.list_of_bin_wthout_key, 'count': 0},
                       'encoded': {'data': self.list_of_mybin, 'count': 0}}
        for list_name, val in dict_analys.items():
            dict_m = {}
            for i in val['data']:
                if i not in dict_m:
                    dict_m[i] = {}
                    dict_m[i]['symbol'] = unichr(int('0b%s' % i, 2))
                    dict_m[i]['bin'] = i
                    dict_m[i]['int'] = int('0b%s' % i, 2)
                    dict_m[i]['count'] = val['data'].count(i)
            dict_analys[list_name]['data'] = list(v for key, v in dict_m.items())
            dict_analys[list_name]['data'] = sorted(dict_analys[list_name]['data'], key=operator.itemgetter('count'), reverse=True)
            dict_analys[list_name]['count'] = len(dict_analys[list_name]['data'])
        dict_analys['count_all'] = len(self.list_of_bin_wthout_key)
        return dict_analys

    #prepaire keyword
    def get_keyword(self, keyword):
        key = []
        if keyword:
            if len(keyword) <= 2:
                keyword_hash = hashlib.md5(keyword).hexdigest()
            elif 2 < len(keyword) <= 4:
                keyword_hash = hashlib.sha256(keyword).hexdigest()
            elif 4 < len(keyword) <= 6:
                keyword_hash = hashlib.sha384(keyword).hexdigest()
            else:
                keyword_hash = hashlib.sha512(keyword).hexdigest()
            keyword = self._str_to_int(keyword_hash)
            keyword = self._int_to_bin(keyword)
            str_k = ''
            for i in xrange(len(keyword_hash)):
                bit = '1'
                for j in keyword[i]:
                    bit = self._sum2(bit, j)
                str_k += bit
                if i == 15 or i == 31 or i == 47 or i == 63 or i == 79 or i == 95 or i == 111 or i == 127:
                    key.append(str_k)
                    str_k = ''
        return key

    def encode_key(self, keyword):
        str_br = ''
        for i, v in enumerate(keyword):
            if i < 15:
                bit = self._sum2(v, keyword[i + 1])
                str_br += bit
                if i == 0:
                    str_br = self._sum2(bit, v) + str_br #0
        return str_br

    # functions for all text
    def _str_to_int(self, text):
        """ input:  str
            output: list of int"""
        return list(ord(i) for i in text)

    def _int_to_bin(self, list_of_int):
        """ input:  list of int
            output: list of bin with base 16 bit"""
        return list(bin(i)[2:].zfill(16) for i in list_of_int)

    def _bin_to_int(self, list_of_bin):
        """ input:  list of bin
            output: list of int"""
        return list(int('0b%s' % i, 2) for i in list_of_bin)

    def _int_to_str(self, list_of_int):
        """ input:  list of int
            output: str with true text"""
        return reduce(lambda x, y: x + unichr(y), list_of_int, '')

    def _bin_to_mybin__(self, list_of_bin):
        res = []
        list_of_bin = self._bin_reverse(list_of_bin)

        first_letter = list_of_bin.pop(0)
        word = ''
        for i, v in enumerate(first_letter):
            if i == 1:
                word += self._sum2(v, first_letter[0])
                word = self._sum2(first_letter[0], word[0]) + word
            elif i <> 0:
                word += self._sum2(v, first_letter[i - 1])
        res.append(word)

        bit = word(len(word) - 1)
        word = ''
        for i, v in enumerate(list_of_bin):
            for ii, vv in enumerate(v):
                if ii < self.range_source:
                    if ii == 0:
                        word += self._sum2(vv, bit)
                    else:
                        word += self._sum2(vv, vv[ii - 1])
                else:
                    word += self._sum2(vv, res[ii][1 + ii])

        res = self._bin_reverse(res)
        return res

    def _bin_to_mybin(self, list_of_bin):
        """ input:  list of bin with source text
            output: list of bin with encoded text (to my code)"""
        list_of_mybin = []
        list_of_bin = self._bin_reverse(list_of_bin)
        last_bit = None
        str_bm_new = ''   #new middle (next)
        str_br = ''   #result
        if list_of_bin:
            str_bm = list_of_bin.pop(0)   #middle
            if list_of_bin:
                for i_list, v_list in enumerate(list_of_bin):
                    for i_str, v_str in enumerate(str_bm):
                        if i_list == 0:
                            if i_str < 15:
                                bit = self._sum2(v_str, str_bm[i_str + 1])
                                str_br += bit
                                if i_str == 0:
                                    str_br = self._sum2(bit, v_str) + str_br #0
                                    last_bit = str_bm[15]
                                str_bm_new += self._sum2(bit, v_list[i_str])
                        else:
                            if i_str == 0:
                                bit = self._sum2(last_bit, v_str)
                                str_br += bit
                                last_bit = self._sum2(bit, list_of_bin[i_list - 1][15])
                                bit_1 = self._sum2(v_str, str_bm[i_str + 1])
                                str_br += bit_1
                                str_bm_new += self._sum2(bit_1, v_list[i_str])
                            elif i_str != 14:
                                bit = self._sum2(v_str, str_bm[i_str + 1])
                                str_br += bit
                                str_bm_new += self._sum2(bit, v_list[i_str])
                            elif i_str == 14:
                                bit = self._sum2(last_bit, v_str)
                                str_br += bit
                                str_bm_new += self._sum2(bit, v_list[i_str])
                    str_bm = str_bm_new
                    str_bm_new = ''
                    list_of_mybin.append(str_br)
                    str_br = ''
                for i_str, v_str in enumerate(str_bm):
                    if i_str == 0 or i_str == 14:
                        bit = self._sum2(last_bit,v_str)
                        str_br += bit
                        if i_str != 14:
                            last_bit = self._sum2(bit, list_of_bin[len(list_of_bin) - 1][15])
                            bit_1 = self._sum2(v_str, str_bm[i_str + 1])
                            str_br += bit_1
                    elif i_str != 14:
                        bit = self._sum2(v_str, str_bm[i_str + 1])
                        str_br += bit
            else:
                for i_str, v_str in enumerate(str_bm):
                    if i_str < 15:
                        bit = self._sum2(v_str, str_bm[i_str + 1])
                        str_br += bit
                        if i_str == 0:
                            str_br = self._sum2(bit, v_str) + str_br #0
            list_of_mybin.append(str_br)
        list_of_mybin = self._bin_reverse(list_of_mybin)
        return list_of_mybin

    def _mybin_to_bin(self, list_of_mybin):
        """ input:  list of bin with encoded text (from my code)
            output: list of bin with source text (decode to source-true text)"""
        list_of_bin = self._bin_reverse(list_of_mybin)
        list_of_mybin = []
        str_br = ''   #result
        str_bm = ''
        pre_bit = None
        for i_list, v_list in enumerate(list_of_bin):
            for i_str, v_str in enumerate(v_list):
                if i_list == 0:
                    if i_str == 0:
                        bit = self._sum2(v_str, v_list[1])
                        str_br += bit                                         #0
                        str_br += self._sum2(v_list[1], bit)                   #1
                    elif i_str > 1:
                        bit = str_br[len(str_br) - 1]
                        bit = self._sum2(v_str, bit)
                        str_br += bit
                else:
                    if not pre_bit:
                        pre_bit = list_of_mybin[i_list - 1][15]
                    if i_str == 0:
                        bit = self._sum2(v_str, pre_bit)
                        str_bm += bit
                        new_bit = self._sum2(bit, list_of_bin[i_list - 1][i_str + 1])
                    elif i_str < 15:
                        bit = self._sum2(str_bm[i_str - 1],v_str)
                        str_bm += bit
                        new_bit = self._sum2(bit, list_of_bin[i_list - 1][i_str + 1])
                    else:                                             # i_str = 15
                        bit = self._sum2(str_bm[i_str - 1], v_str)
                        str_bm += bit
                        new_bit = self._sum2(bit, v_list[0])
                    str_br += new_bit
            list_of_mybin.append(str_br)
            str_br = ''
            if str_bm != '':
                pre_bit = str_bm[15]
            str_bm = ''
        list_of_mybin = self._bin_reverse(list_of_mybin)
        return list_of_mybin

    def _bin_reverse(self, list_of_bin):
        """ 1010  --->  0101 """
        new_list_of_bin = []
        for i in list_of_bin:
            new_str = ''
            for j in i:
                new_str = j + new_str
            new_list_of_bin.append(new_str)
        return new_list_of_bin

    def _sum2(self, a, b):
        """ a,b - char '0' or '1'.
            result = sum by mod2."""
        return str(int(a)^int(b))