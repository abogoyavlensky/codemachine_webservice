var codemachine;


codemachine = {
    init: function() {
        codemachine.funcs.init();
        $('.decode-btn').click(function(){
            Dajaxice.codemachine.decode_text(Dajax.process,{'encoded_text':$('#id_source_text').text(),
//                                                            'range_source': $('select[name="range"]').val()
                                                            'range_source': 0, 'keyword': $('#id_keyword').val(),
                                                            'true_key': $('#true_key').data('true_key')
            });
        });
        var pair = $('#pair_data').data('chartData');
        $('#pair_stat').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Amount pairs'
            },
            xAxis: {
                categories: pair['encoded']['point']
            },
            yAxis: {
                title: {
                    text: 'Amount'
                }
            },
            series: [{
                name: pair['encoded']['name'],
                data: pair['encoded']['val']
            },{
                name: pair['source']['name'],
                data: pair['source']['val']
            }
            ]
        });
        var triplet = $('#triplet_data').data('chartData');
        $('#triplet_stat').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Amount triplets'
            },
            xAxis: {
                categories: triplet['encoded']['point']
            },
            yAxis: {
                title: {
                    text: 'Amount'
                }
            },
            series: [{
                name: triplet['encoded']['name'],
                data: triplet['encoded']['val']
            },{
                name: triplet['source']['name'],
                data: triplet['source']['val']
            }]
        });
        var dist = $('#dist_data').data('chartData');
        if ($('#dist_stat').length){
            $('#dist_stat').highcharts({
                chart: {
                    type: 'line',
                    marginRight: 130,
                    marginBottom: 25
                },
                title: {
                    text: 'The distribution of zeros on amount of bits',
                    x: -20 //center
                },
                xAxis: {
                    title: {
                        text: 'Amount of bits'
                    },
                    categories: dist['encoded']['count']
                },
                yAxis: {
                    title: {
                        text: 'Distribution of zero'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -10,
                    y: 100,
                    borderWidth: 0
                },
                series: [{
                    name: dist['encoded']['name'],
                    data: dist['encoded']['val']
                },{
                    name: dist['source']['name'],
                    data: dist['source']['val']
                }
                ]
            });
        }

        var dist_one = $('#dist_one_data').data('chartData');
        if ($('#dist_one_stat').length){
            $('#dist_one_stat').highcharts({
                chart: {
                    type: 'line',
                    marginRight: 130,
                    marginBottom: 25
                },
                title: {
                    text: 'The distribution of ones on amount of bits',
                    x: -20 //center
                },
                xAxis: {
                    title: {
                        text: 'Amount of bits'
                    },
                    categories: dist_one['encoded']['count']
                },
                yAxis: {
                    title: {
                        text: 'Distribution of one'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: ''
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -10,
                    y: 100,
                    borderWidth: 0
                },
                series: [{
                    name: dist_one['encoded']['name'],
                    data: dist_one['encoded']['val']
                },{
                    name: dist_one['source']['name'],
                    data: dist_one['source']['val']
                }
                ]
            });
        }
    },

    funcs: {
        init: function(){
        },
        set_decoded_text: function(data){
            $('#id_encoded_text').text(data['decoded_text']);
        }
    }
};

$(function() {
    codemachine.init();
});
