__author__ = 'Andrey Bogoyavlensky'

from django import forms
from django.utils.translation import ugettext_lazy as _


class GetTextForm(forms.Form):
    source_text = forms.CharField(widget=forms.Textarea(attrs={'rows': 10,
                                                        'class': 'span12',
                                                        'placeholder': 'Please enter your text...'}),
                           initial='', required=True, label=_('Enter source text'))
    keyword = forms.CharField(required=False, label='Key', max_length=30)

    def __init__(self, *args, **kwargs):
        super(GetTextForm, self).__init__(*args, **kwargs)


class EncodeTextForm(forms.Form):
    encoded_text = forms.CharField(widget=forms.Textarea(attrs={'rows': 10,
                                                        'class': 'span12',
                                                        'readonly': 'readonly',
                                                        'placeholder': 'Space for encoded text...'}),
                           initial='', required=False, label=_('Encoded text'))

    def __init__(self, *args, **kwargs):
        super(EncodeTextForm, self).__init__(*args, **kwargs)