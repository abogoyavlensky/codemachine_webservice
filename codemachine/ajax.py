__author__ = 'moscow'
from dajaxice.decorators import dajaxice_register
from dajax.core import Dajax
from codemachine.utils import Coder


@dajaxice_register
def decode_text(request, encoded_text, range_source, keyword, true_key):
    dajax = Dajax()
    coder = Coder()
    encoded_text = coder.get_encode_text(encoded_text, range_source, true_key)
    decoded_text = coder.get_decode_text(encoded_text, keyword)
    dajax.add_data({'decoded_text': decoded_text}, 'codemachine.funcs.set_decoded_text')
    return dajax.json()
