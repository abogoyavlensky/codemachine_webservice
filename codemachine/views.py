# Create your views here.
from codemachine.forms import GetTextForm, EncodeTextForm
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from codemachine.utils import Coder
import simplejson as json


def home(request):
    true_key = ''
    if request.method == 'POST':
        form_source = GetTextForm(request.POST)
        if form_source.is_valid():
            coder = Coder()
            true_key = form_source.cleaned_data.get('keyword')
            encoded_text = coder.get_encode_text(request.POST['source_text'], 0, true_key)
            dist = json.dumps(coder.get_zero_distribution())
            dist_one = json.dumps(coder.get_zero_distribution('1'))
            pair = json.dumps(coder.get_bit_distribution(['11', '10', '01', '00'], 'Pairs'))
            triplet = json.dumps(coder.get_bit_distribution(['111', '110', '101', '100', '011', '010', '001', '000'], 'Triplets'))
            analys_statistic = coder.analys_statistic()
            form_encoded = EncodeTextForm(initial={'encoded_text': encoded_text})
        else:
            dist = None
            dist_one = None
            pair = None
            triplet = None
            analys_statistic = None
            form_encoded = EncodeTextForm()
    else:
        dist = None
        dist_one = None
        pair = None
        triplet = None
        analys_statistic = None
        form_source = GetTextForm()
        form_encoded = EncodeTextForm()
    # range_select = request.POST.get('range')
    return render(request, 'codemachine/main.html', {'form_source': form_source,
                                                     'form_encoded': form_encoded,
                                                     # 'ranges': [i for i in xrange(16)],
                                                     # 'range_select': int(range_select) if range_select else 0,
                                                     'dist': dist,
                                                     'dist_one': dist_one,
                                                     'pair': pair,
                                                     'triplet': triplet,
                                                     'true_key': true_key,
                                                     'analys': analys_statistic})


def clear_text(request):
    return redirect(reverse('home'))


def about(request):
    return render(request, 'codemachine/about.html')