__author__ = 'moscow'

from django.conf.urls import patterns, include, url

urlpatterns = patterns('codemachine.views',
    url(r'^$', 'home', name='home'),
    url(r'^about$', 'about', name='about'),
    url(r'^clear$', 'clear_text', name='clear_text'),
)
